#define PLUGIN_AUTHOR ".#Zipcore"
#define PLUGIN_NAME "Z-Core: Loot Spawner"
#define PLUGIN_VERSION "3.9.3"
#define PLUGIN_DESCRIPTION "Allows other plugins to register loot groups which make use of the spawns managed by this plugin"
#define PLUGIN_URL "zipcore.net"

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>

#include <smlib>
#include <map_workshop_functions> // Adds workshop map support

#include <zcore/zcore_lootspawner>

public Plugin myinfo = 
{
	name = PLUGIN_NAME, 
	author = PLUGIN_AUTHOR, 
	description = PLUGIN_DESCRIPTION, 
	version = PLUGIN_VERSION, 
	url = PLUGIN_URL
}

/* Macros */

#define LoopClients(%1) for(int %1 = 1; %1 <= MaxClients; %1++)

#define LoopIngameClients(%1) for(int %1=1;%1<=MaxClients;%1++)\
if (IsClientInGame(%1))
	
#define LoopIngamePlayers(%1) for(int %1=1;%1<=MaxClients;%1++)\
if (IsClientInGame(%1) && !IsFakeClient(%1))
	
#define LoopAlivePlayers(%1) for(int %1=1;%1<=MaxClients;%1++)\
if (IsClientInGame(%1) && IsPlayerAlive(%1))

/* ConVars */

ConVar g_cvCheckInterval;
float g_fCheckInterval;

ConVar g_cvSpawnsMax;
int g_iSpawnsMax;

ConVar g_cvSpawnsMaxRoundStart;
int g_iSpawnsMaxRoundStart;

ConVar g_cvMinDistance;
float g_fMinDistance;

/* Timer */

bool g_bIdle;
Handle g_hTimer = null;

/* Forwards */

Handle g_OnPluginStart;
Handle g_OnLootSpawnThink;
Handle g_OnLootSpawnPre;
Handle g_OnLootSpawned;
Handle g_OnLootAction;
Handle g_OnLootRemoved;

/* Player Spawns */

Handle g_aSpawns = null;
float g_fSpawnCenter[3];

/* Loot */

enum Loot
{
	LootGroup, 
	LootSpawnID,
	LootFlags,
	LootSpawned
}

enum LootSpawn
{
	bool:SpawnIgnore, 
	Float:SpawnPosX, 
	Float:SpawnPosY, 
	Float:SpawnPosZ, 
	bool:SpawnInUse, 
	SpawnEntity
}

int g_nLoot[2048][Loot]; // Entity to SpawnID

/* Loot Spawns */

int g_iLootSpawnsCount; // Amount of lootspawns cached
int g_iLootSpawnsAvailable; // Without the ignored ones

int g_nLootSpawns[MAX_LOOTSPAWNS][LootSpawn]; // Lootspawn cache

/* Loot Groups */

Handle g_aLootGroups = null; // name

Handle g_aLootModels = null; // path

Handle g_aLootWeight = null; // weight
Handle g_aLootWeightRoundStart = null; // weight for round start

Handle g_aLootTable = null; // weight
Handle g_aLootTableRoundStart = null; // weight for round start

Handle g_aMaxSpawned = null; // max spawned

Handle g_aSpawnDistanceMin = null; // Min distance to player spawn (center)
Handle g_aSpawnDistanceMax = null; // Max distnace to palyer spawn (center)

Handle g_aTTL = null; // Time to life
Handle g_aHealth = null; // Health to spawn with

Handle g_aFlags = null; // flags

/* Sprites */

int g_iGlow;

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	RegPluginLibrary("zcore-lootspawner");
	
	CreateNatives();
	CreateForwards();
	CreateArrays();
	
	return APLRes_Success;
}

public void OnPluginStart()
{
	/* Event Hooks*/
	HookEvents();
	
	/* Admin Commands */
	RegisterAdminCommands();
	
	/* Convars */
	CreateConVars();
	
	Call_StartForward(g_OnPluginStart);
	Call_Finish();
}

public void OnMapStart()
{
	LoadLootSpawns();
	PrecacheModels();
	RestartTimer();
}

public void OnMapEnd()
{
	g_hTimer = null;
	LootDeleteAll();
	g_bIdle = true;
	g_iLootSpawnsCount = 0;
}

/* Hooked Events */

void HookEvents()
{
	HookEvent("round_end", Event_RoundEnd);
	HookEvent("round_start", Event_RoundStart);
	
	HookEvent("player_spawn", Event_CheckIdleMode);
	HookEvent("round_freeze_end", Event_CheckIdleMode);
}

public Action Event_CheckIdleMode(Handle event, const char[] name, bool dontBroadcast)
{
	if(GameRules_GetProp("m_bWarmupPeriod") == 0)
	{
		if(g_bIdle)
		{
			g_bIdle = false;
			CheckSpawns(true); // Roundstart loot routine
		}
	}
	else g_bIdle = true;
	
	return Plugin_Continue;
}

public Action Event_RoundStart(Handle event, const char[] name, bool dontBroadcast)
{
	LootDeleteAll();
	
	return Plugin_Continue;
}

public Action Event_RoundEnd(Handle event, const char[] name, bool dontBroadcast)
{
	LootDeleteAll();
	g_bIdle = true;
	
	return Plugin_Continue;
}

/* Admin Commands */

void RegisterAdminCommands()
{
	RegAdminCmd("zcore_loot", Cmd_Menu, ADMFLAG_ROOT);
	RegAdminCmd("zcore_loot_add", Cmd_AddLootSpawn, ADMFLAG_ROOT);
	RegAdminCmd("zcore_loot_show", Cmd_ShowLootSpawns, ADMFLAG_ROOT);
	RegAdminCmd("zcore_loot_delete", Cmd_DeleteLootSpawn, ADMFLAG_ROOT);
	RegAdminCmd("zcore_loot_delete_prev", Cmd_DeletePrevLootSpawn, ADMFLAG_ROOT);
	RegAdminCmd("zcore_loot_reload", Cmd_ReloadLoot, ADMFLAG_ROOT);
	RegAdminCmd("zcore_loot_debug", Cmd_DebugLoot, ADMFLAG_ROOT);
}

public Action Cmd_Menu(int client, int args)
{
	Menu menu = new Menu(Handler_Menu);
	menu.SetTitle("Z-Core Lootspawner by Zipcore");
	
	menu.AddItem("add", "Add spawn (cur. pos.)");
	menu.AddItem("show", "Highlight near spawns");
	menu.AddItem("delete", "Delete spawn (nearest)");
	menu.AddItem("delete_prev", "Delete spawn (last)");
	menu.AddItem("reload", "Respawn all loot");
	menu.AddItem("debug", "Debug (see console)");
	
	SetMenuExitButton(menu, true);
	menu.Display(client, MENU_TIME_FOREVER);
	
	return Plugin_Handled;
}

public int Handler_Menu(Menu menu, MenuAction action, int client, int params)
{
	if (action == MenuAction_Select)
	{
		char sInfo[16];
		menu.GetItem(params, sInfo, sizeof(sInfo));
		
		if(StrEqual(sInfo, "add"))
		{
			Cmd_AddLootSpawn(client, 0);
			Cmd_Menu(client, 0);
		}
		else if(StrEqual(sInfo, "show"))
		{
			Cmd_ShowLootSpawns(client, 0);
			Cmd_Menu(client, 0);
		}
		else if(StrEqual(sInfo, "delete"))
		{
			Cmd_DeleteLootSpawn(client, 0);
			Cmd_Menu(client, 0);
		}
		else if(StrEqual(sInfo, "delete_prev"))
		{
			Cmd_DeletePrevLootSpawn(client, 0);
			Cmd_Menu(client, 0);
		}
		else if(StrEqual(sInfo, "reload"))
		{
			Cmd_ReloadLoot(client, 0);
			Cmd_Menu(client, 0);
		}
		else if(StrEqual(sInfo, "debug"))
		{
			Cmd_DebugLoot(client, 0);
			Cmd_Menu(client, 0);
		}
	}
	else if (action == MenuAction_End)
		delete menu;
}

public Action Cmd_AddLootSpawn(int client, int args)
{
	AddLootSpawn(client);
	
	return Plugin_Handled;
}

public Action Cmd_ShowLootSpawns(int client, int args)
{
	ShowLootSpawns(client);
	
	return Plugin_Handled;
}

public Action Cmd_DeleteLootSpawn(int client, int args)
{
	DeleteLootSpawn(client);
	
	return Plugin_Handled;
}

public Action Cmd_DeletePrevLootSpawn(int client, int args)
{
	DeletePrevLootSpawn(client);
	
	return Plugin_Handled;
}

public Action Cmd_ReloadLoot(int client, int args)
{
	LootDeleteAll();
	LoadLootSpawns();
	CheckSpawns(true);
	
	return Plugin_Handled;
}

public Action Cmd_DebugLoot(int client, int args)
{
	PrintToConsole(client, "### ZCore-Lootspawner: Debug START ###");
	PrintToConsole(client, "Lootspawns: %d (%d marked for delete)", g_iLootSpawnsCount, g_iLootSpawnsCount-g_iLootSpawnsAvailable);
	PrintToConsole(client, "Loot spawned: %d", GetLootCount());
	PrintToConsole(client, "### ZCore-Lootspawner: Lootgroups (%d) ###", GetArraySize(g_aLootGroups));
	
	for (int i = 0; i < GetArraySize(g_aLootGroups); i++)
	{
		char name[32];
		GetArrayString(g_aLootGroups, i, name, 32);
		PrintToConsole(client, "Name: %s", name);
		
		int weight = GetArrayCell(g_aLootWeight, i);
		int weightRS = GetArrayCell(g_aLootWeightRoundStart, i);
		
		int table = GetArraySize(g_aLootTable);
		int tableRS = GetArraySize(g_aLootTableRoundStart);
		
		float chance = float(weight)/float(table)*100.0;
		float chanceRS = float(weightRS)/float(tableRS)*100.0;
		
		PrintToConsole(client, "- Spawn weight: %d RS: %d", weight, weightRS);
		PrintToConsole(client, "- Spawn chance: %.2f%% RS: %.2f%%", chance, chanceRS);
		PrintToConsole(client, "- Spawned: %d/%d", GetLootCountByGroup(i), GetArrayCell(g_aMaxSpawned, i));
		
		char model[256];
		GetArrayString(g_aLootModels, i, model, 256);
		PrintToConsole(client, "- Model: \"%s\"", model);
	}
	
	PrintToConsole(client, "### ZCore-Lootspawner: Debug END ###");
	
	return Plugin_Handled;
}

/* Manage Loot Groups */

void CreateArrays()
{
	g_aLootGroups = CreateArray(32);
	g_aLootModels = CreateArray(256);
	g_aLootWeight = CreateArray(1);
	g_aLootWeightRoundStart = CreateArray(1);
	g_aLootTable = CreateArray(1);
	g_aLootTableRoundStart = CreateArray(1);
	g_aMaxSpawned = CreateArray(1);	
	g_aSpawnDistanceMin = CreateArray(1);	
	g_aSpawnDistanceMax = CreateArray(1);	
	g_aTTL = CreateArray(1);	
	g_aHealth = CreateArray(1);	
	g_aFlags = CreateArray(1);	
}

void PrecacheModels()
{
	g_iGlow = PrecacheModel("sprites/blueglow1.vmt");
	
	for (int i = 0; i < GetArraySize(g_aLootModels); i++)
	{
		char model[256];
		GetArrayString(g_aLootModels, i, model, 256);
		
		if (!IsModelPrecached(model))
			PrecacheModel(model, true);
	}	
}

/* Manage Loot Spawns */

void LoadLootSpawns()
{
	g_iLootSpawnsCount = 0;
	g_iLootSpawnsAvailable = 0;
	
	char sRawMap[PLATFORM_MAX_PATH];
	char sMap[64];
	GetCurrentMap(sRawMap, sizeof(sRawMap));
	RemoveMapPath(sRawMap, sMap, sizeof(sMap));
	
	char sPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, sPath, sizeof(sPath), "configs/lootspawns/%s.txt", sMap);
	
	Handle hFile = OpenFile(sPath, "r");
	
	char sBuffer[512];
	char sDatas[3][32];
	
	if (hFile != null)
	{
		while (ReadFileLine(hFile, sBuffer, sizeof(sBuffer)))
		{
			ExplodeString(sBuffer, ";", sDatas, 3, 32);
			
			g_nLootSpawns[g_iLootSpawnsCount][SpawnPosX] = StringToFloat(sDatas[0]);
			g_nLootSpawns[g_iLootSpawnsCount][SpawnPosY] = StringToFloat(sDatas[1]);
			g_nLootSpawns[g_iLootSpawnsCount][SpawnPosZ] = StringToFloat(sDatas[2]);
			
			g_nLootSpawns[g_iLootSpawnsCount][SpawnInUse] = false;
			g_nLootSpawns[g_iLootSpawnsCount][SpawnIgnore] = false;
			g_nLootSpawns[g_iLootSpawnsCount][SpawnEntity] = 0;
			
			g_iLootSpawnsCount++;
			g_iLootSpawnsAvailable++;
		}
		
		CloseHandle(hFile);
	}
}

void SaveLootSpawns()
{
	char sRawMap[PLATFORM_MAX_PATH];
	char sMap[64];
	GetCurrentMap(sRawMap, sizeof(sRawMap));
	RemoveMapPath(sRawMap, sMap, sizeof(sMap));
	
	char sPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, sPath, sizeof(sPath), "configs/lootspawns/%s.txt", sMap);
	
	Handle hFile = OpenFile(sPath, "w");
	
	if (hFile != null)
	{
		for (int i = 0; i < g_iLootSpawnsCount; i++)
		{
			if(g_nLootSpawns[i][SpawnIgnore])
				continue;
			
			WriteFileLine(hFile, "%.2f;%.2f;%.2f;", g_nLootSpawns[i][SpawnPosX], g_nLootSpawns[i][SpawnPosY], g_nLootSpawns[i][SpawnPosZ]);
		}
		
		CloseHandle(hFile);
	}
	
	if(!FileExists(sPath))
		LogError("Couldn't save loot spawns to  file: \"%s\".", sPath);
}

void ShowLootSpawns(int client)
{
	int iCount;
	
	float fClientPos[3];
	GetClientAbsOrigin(client, fClientPos);
	
	for (int i = 0; i < g_iLootSpawnsCount; i++)
	{
		if (iCount > 50)
		{
			PrintToChat(client, "Can't show all lootspawns in range.");
			break;
		}
			
		if (g_nLootSpawns[i][SpawnIgnore])
			continue;
		
		float fSpawnPos[3];
			
		fSpawnPos[0] = g_nLootSpawns[i][SpawnPosX];
		fSpawnPos[1] = g_nLootSpawns[i][SpawnPosY];
		fSpawnPos[2] = g_nLootSpawns[i][SpawnPosZ];
			
		if (GetVectorDistance(fClientPos, fSpawnPos) < 2048.0)
		{
			TE_SetupGlowSprite(fSpawnPos, g_iGlow, 10.0, 1.0, 235);
			TE_SendToAll();
			iCount++;
		}
	}
	
	if(iCount == 0)
		PrintToChat(client, "No lootspawn found which is in range.");
	else PrintToChat(client, "Marked %d lootspawn which are in range.", iCount);
}

void AddLootSpawn(int client)
{
	float pos[3];
	GetClientAbsOrigin(client, pos);
	
	TE_SetupGlowSprite(pos, g_iGlow, 10.0, 1.0, 235);
	TE_SendToAll();
	
	g_nLootSpawns[g_iLootSpawnsCount][SpawnPosX] = pos[0];
	g_nLootSpawns[g_iLootSpawnsCount][SpawnPosY] = pos[1];
	g_nLootSpawns[g_iLootSpawnsCount][SpawnPosZ] = pos[2];
	
	g_iLootSpawnsCount++;
	g_iLootSpawnsAvailable++;
	
	PrintToChat(client, "Added new loot spawn at %.2f:%.2f:%.2f, counting %d loot spawns.", pos[0], pos[1], pos[2], g_iLootSpawnsAvailable);
	
	SaveLootSpawns();
}

void DeletePrevLootSpawn(int client)
{
	g_iLootSpawnsCount--;
	g_iLootSpawnsAvailable--;
	
	PrintToChat(client, "Deleted last loot spawn, counting %d loot spawns.", g_iLootSpawnsAvailable);
	
	SaveLootSpawns();
}

void DeleteLootSpawn(int client)
{
	float fClientPos[3];
	GetClientAbsOrigin(client, fClientPos);
	
	float fMinDist = -1.0;
	int iIndex = -1;
	
	for (int i = 0; i < g_iLootSpawnsCount; i++)
	{
		if (g_nLootSpawns[i][SpawnIgnore])
			continue;
		
		float fSpawnPos[3];
			
		fSpawnPos[0] = g_nLootSpawns[i][SpawnPosX];
		fSpawnPos[1] = g_nLootSpawns[i][SpawnPosY];
		fSpawnPos[2] = g_nLootSpawns[i][SpawnPosZ];
		
		float fDist = GetVectorDistance(fClientPos, fSpawnPos);
		if (fDist > 512.0)
			continue;
			
		if (fMinDist == -1.0 || fDist < fMinDist)
		{
			fMinDist = fDist;
			iIndex = i;
		}
	}
	
	if(iIndex != -1)
	{
		float fSpawnPos[3];
			
		fSpawnPos[0] = g_nLootSpawns[iIndex][SpawnPosX];
		fSpawnPos[1] = g_nLootSpawns[iIndex][SpawnPosY];
		fSpawnPos[2] = g_nLootSpawns[iIndex][SpawnPosZ];
		
		PrintToChat(client, "Deleted nearest loot spawn at [%.2f|%.2f|%.2f].", fSpawnPos[0], fSpawnPos[1], fSpawnPos[2]);
		g_nLootSpawns[iIndex][SpawnIgnore] = true;
		g_iLootSpawnsAvailable--;
		SaveLootSpawns();
	}
	else PrintToChat(client, "No lootspawn found which is in range.");
}

// How luch loot we can spawn
int GetMaxSpawnableLoot()
{
	int iMax;
	for (int i = 0; i < GetArraySize(g_aLootGroups); i++)
		iMax += GetArrayCell(g_aMaxSpawned, i);
	
	return iMax;
}

/* Manage Loot */

// Get how many loot has been spawned
int GetLootCount()
{
	int count;
	for (int i = 0; i < g_iLootSpawnsCount; i++)
	{
		if (g_nLootSpawns[i][SpawnInUse])
			count++;
	}
	return count;
}

// Get how much loot are spawned for given loot group
int GetLootCountByGroup(int iGroup)
{
	int count;
	for (int i = 0; i < g_iLootSpawnsCount; i++)
	{
		if (g_nLootSpawns[i][SpawnInUse] && g_nLoot[g_nLootSpawns[i][SpawnEntity]][LootGroup] == iGroup)
			count++;
	}
	return count;
}

void LootDeleteAll()
{
	if (g_iLootSpawnsCount <= 0)
		return;
	
	for (int entity = MAXPLAYERS; entity < 2047; entity++)
	{
		if (g_nLoot[entity][LootGroup] != TYPE_NONE && IsValidEntity(entity))
			AcceptEntityInput(entity, "Kill");
		
		g_nLoot[entity][LootGroup] = TYPE_NONE;
		g_nLoot[entity][LootSpawnID] = 0;
		g_nLoot[entity][LootFlags] = 0;
		g_nLoot[entity][LootSpawned] = 0;
	}
	
	for (int i = 0; i < g_iLootSpawnsCount; i++)
	{
		g_nLootSpawns[i][SpawnInUse] = false;
		g_nLootSpawns[i][SpawnEntity] = 0;
	}
}

void LootDeleteGroup(int iGroup)
{
	if (g_iLootSpawnsCount <= 0)
		return;
	
	for (int i = 0; i < g_iLootSpawnsCount; i++)
	{
		int entity = g_nLootSpawns[i][SpawnEntity];
		
		if (g_nLoot[entity][LootGroup] != iGroup)
			continue;
		
		if (IsValidEntity(entity))
			AcceptEntityInput(entity, "Kill");
		
		g_nLoot[entity][LootGroup] = TYPE_NONE;
		g_nLoot[entity][LootSpawnID] = 0;
		g_nLoot[entity][LootFlags] = 0;
		g_nLoot[entity][LootSpawned] = 0;
		
		g_nLootSpawns[i][SpawnInUse] = false;
		g_nLootSpawns[i][SpawnEntity] = 0;
	}
}

void LootDelete(int iEntity, int iClient, int iAction)
{
	Action result = OnLootRemoved(iClient, iEntity, g_nLoot[iEntity][LootGroup], iAction);
	
	if(result == Plugin_Stop || result == Plugin_Handled)
		return;
	
	if (IsValidEntity(iEntity))
		AcceptEntityInput(iEntity, "Kill");
	
	g_nLoot[iEntity][LootGroup] = TYPE_NONE;
	
	int iSpawnID = g_nLoot[iEntity][LootSpawnID];
	
	if(iSpawnID >= 0)
	{
		g_nLootSpawns[iSpawnID][SpawnInUse] = false;
		g_nLootSpawns[iSpawnID][SpawnEntity] = 0;
	}
	
	g_nLoot[iEntity][LootSpawnID] = 0;
	g_nLoot[iEntity][LootFlags] = 0;
	g_nLoot[iEntity][LootSpawned] = 0;
}

void CheckPlayerSpawns()
{
	float fPos[3];
	int iCount;
	
	if(g_aSpawns != null)
		ClearArray(g_aSpawns);
	else g_aSpawns = CreateArray(3);
	
	int	entity;
	while ((entity = FindEntityByClassname(entity, "info_player_counterterrorist" )) != -1)
	{
		Entity_GetAbsOrigin(entity, fPos);
		AddVectors(g_fSpawnCenter, fPos, g_fSpawnCenter);
		PushArrayArray(g_aSpawns, fPos, 3);
		iCount++;
	}
	
	while ((entity = FindEntityByClassname(entity, "info_player_terrorist" )) != -1)
	{
		Entity_GetAbsOrigin(entity, fPos);
		AddVectors(g_fSpawnCenter, fPos, g_fSpawnCenter);
		PushArrayArray(g_aSpawns, fPos, 3);
		iCount++;
		
	}
	
	if(iCount > 1)
	{
		g_fSpawnCenter[0] /= iCount;
		g_fSpawnCenter[1] /= iCount;
		g_fSpawnCenter[2] /= iCount;
	}
}

void CheckSpawns(bool bRoundstart)
{
	if(g_iLootSpawnsCount <= 0 || g_aLootGroups == null || GetArraySize(g_aLootGroups) <= 0)
		return;
	
	if(bRoundstart)
	{
		LootDeleteAll();
		CheckPlayerSpawns();
	}
	else if(g_aTTL != null && GetArraySize(g_aTTL) > 0)
	{
		for (int iEntity = MAXPLAYERS; iEntity < 2047; iEntity++)
		{
			if(g_nLoot[iEntity][LootGroup] == TYPE_NONE)
				continue;
			
			if(g_nLoot[iEntity][LootGroup] >= GetArraySize(g_aTTL))
			{
				g_nLoot[iEntity][LootGroup] = TYPE_NONE;
				continue;
			}
			
			int iTTL = GetArrayCell(g_aTTL, g_nLoot[iEntity][LootGroup]);
			
			if(iTTL > 0 && GetTime()-g_nLoot[iEntity][LootSpawned] > iTTL)
				LootDelete(iEntity, 0, LS_ACTION_TTL);
		}
	}
	
	int iSpawnsToFill;
	
	if(bRoundstart)
		iSpawnsToFill = g_iSpawnsMaxRoundStart;
	else iSpawnsToFill = g_iSpawnsMax;
	
	// Check available spawn points
	if (g_iLootSpawnsAvailable < iSpawnsToFill)
		iSpawnsToFill = g_iLootSpawnsAvailable;
	
	// Check available loot
	int iLootAvalable = GetMaxSpawnableLoot();
	if (iLootAvalable < iSpawnsToFill)
		iSpawnsToFill = iLootAvalable;
	
	// Check amount of already filled spawns
	int iSpawnsFilled = GetLootCount();
	if (iSpawnsFilled < iSpawnsToFill)
		iSpawnsToFill -= iSpawnsFilled;
	else return; // Already spawned enough loot
	
	int iFailStreak;
	
	// Fill loot spawn until failure
	while(iSpawnsFilled < iSpawnsToFill && iFailStreak < 100)
	{
		int iLootGroup = GetRandomLootGroup(bRoundstart);
		if(iLootGroup == -1)
		{
			iFailStreak++;
			continue;
		}
		
		Action result = OnLootSpawnThink(iLootGroup, iFailStreak);
		if(result == Plugin_Handled || result == Plugin_Handled)
		{
			iFailStreak++;
			continue;
		}
		
		int iSpawnID = GetFreeRandomSpawn(iLootGroup);
		if(iSpawnID == -1)
		{
			iFailStreak++;
			continue;
		}
		
		float fPos[3];
		fPos[0] = g_nLootSpawns[iSpawnID][SpawnPosX];
		fPos[1] = g_nLootSpawns[iSpawnID][SpawnPosY];
		fPos[2] = g_nLootSpawns[iSpawnID][SpawnPosZ] + 32.0;
		
		Action result2 = OnLootSpawnPre(iLootGroup, fPos, iFailStreak);
		if(result2 == Plugin_Handled || result2 == Plugin_Handled)
		{
			iFailStreak++
			continue;
		}
		
		if(SpawnLoot(iSpawnID, iLootGroup))
		{
			iSpawnsFilled++;
			iFailStreak = 0;
		}
	}
}

int GetRandomLootGroup(bool bRoundstart)
{
	int iMax = -1;
	
	if (bRoundstart)
		iMax = GetArraySize(g_aLootTableRoundStart);
	else iMax = GetArraySize(g_aLootTable);
	
	// There is no loot to spawn
	if (iMax <= 0)
		return -1;
	
	int iCurrent = GetRandomInt(0, iMax-1);
	int iGroup = -1;
	
	for (int i = 0; i < iMax; i++)
	{
		if(iCurrent >= iMax)
			iCurrent = 0;
			
		if(bRoundstart)
			iGroup = GetArrayCell(g_aLootTableRoundStart, iCurrent);
		else iGroup = GetArrayCell(g_aLootTable, iCurrent);
		
		int iGroupMax = GetArrayCell(g_aMaxSpawned, iGroup);
		int iGroupSpawned = GetLootCountByGroup(iGroup);
		
		// Finally we found a loot groups which is allowed to be spawned
		if (iGroupSpawned < iGroupMax)
			break;
		
		iCurrent++;
	}
	
	return iGroup;
}

int GetFreeRandomSpawn(int iGroup)
{
	int iCurrent = GetRandomInt(0, g_iLootSpawnsCount - 1);
	
	for (int i = 0; i < g_iLootSpawnsCount; i++)
	{
		if(iCurrent >= g_iLootSpawnsCount)
			iCurrent = 0;
		
		// Just spawn just got deleted or is already in use
		if (g_nLootSpawns[iCurrent][SpawnIgnore] || g_nLootSpawns[iCurrent][SpawnInUse])
		{
			iCurrent++;
			continue;
		}
		
		int iFlags = GetArrayCell(g_aFlags, iGroup);
		
		float pos[3];
		pos[0] = g_nLootSpawns[iCurrent][SpawnPosX];
		pos[1] = g_nLootSpawns[iCurrent][SpawnPosY];
		pos[2] = g_nLootSpawns[iCurrent][SpawnPosZ];
		
		// Check distance to neaest player
		
		if (g_fMinDistance > 0.0)
		{
			float fPlayerDistance = GetClosestPlayerDistance(pos);
			if (fPlayerDistance != -1.0 && fPlayerDistance < g_fMinDistance)
			{
				iCurrent++;
				continue;
			}
		}
		
		// Check distance to player spawns
		
		float fSpawnDistance = GetVectorDistance(pos, g_fSpawnCenter);
		
		float rangeMin = float(GetArrayCell(g_aSpawnDistanceMin, iGroup));
		if(rangeMin > 0.0)
		{
			if(iFlags & LS_FLAG_USESPAWNCENTER)
			{
				if(fSpawnDistance < rangeMin)
				{
					iCurrent++;
					continue;
				}
			}
			else
			{
				float fDistMin = -1.0;
				for (int j = 0; j < GetArraySize(g_aSpawns); j++)
				{
					float fPos[3];
					GetArrayArray(g_aSpawns, j, fPos, 3);
					
					float fDist = GetVectorDistance(pos, fPos);
					if(fDist == -1.0 || fDist < fDistMin)
						fDistMin = fDist;
				}
				
				if(fDistMin < rangeMin)
				{
					iCurrent++;
					continue;
				}
			}
		}
		
		float rangeMax = float(GetArrayCell(g_aSpawnDistanceMax, iGroup));
		if(rangeMax > 0.0)
		{
			if(iFlags & LS_FLAG_USESPAWNCENTER)
			{
				if(fSpawnDistance > rangeMax)
				{
					iCurrent++;
					continue;
				}
			}
			else
			{
				float fDistMax = -1.0;
				for (int j = 0; j < GetArraySize(g_aSpawns); j++)
				{
					float fPos[3];
					GetArrayArray(g_aSpawns, j, fPos, 3);
					
					float fDist = GetVectorDistance(pos, fPos);
					if(fDist == -1.0 || fDist > fDistMax)
						fDistMax = fDist;
				}
				
				if(fDistMax > rangeMax)
				{
					iCurrent++;
					continue;
				}
			}
		}
		
		return iCurrent;
	}
	
	return -1;
}

int GetSpawnNear(float fPos[3], float fRangeMin, float fRangeMax, bool bNextFree, bool bRemove)
{
	int iRandomSpawnId = GetRandomInt(0, g_iLootSpawnsCount - 1);
	int iCurrent = iRandomSpawnId-1;
	
	int iNearest = -1;
	float fNearestDis;
	
	// Get a free slot
	for (int i = 0; i < g_iLootSpawnsCount; i++)
	{
		iCurrent++;
		
		if(iCurrent >= g_iLootSpawnsCount)
			iCurrent = 0;
			
		if (g_nLootSpawns[iCurrent][SpawnIgnore])
			continue;
		
		if (bNextFree && g_nLootSpawns[iCurrent][SpawnInUse])
			continue;
		
		float fSpawnPos[3];
		fSpawnPos[0] = g_nLootSpawns[iCurrent][SpawnPosX];
		fSpawnPos[1] = g_nLootSpawns[iCurrent][SpawnPosY];
		fSpawnPos[2] = g_nLootSpawns[iCurrent][SpawnPosZ];
		
		float fDistance = GetVectorDistance(fSpawnPos, fPos);
		
		if(fDistance > fRangeMax)
			continue;
		
		if(fDistance < fRangeMin)
			continue;
		
		if(iNearest == -1 || fNearestDis > fDistance)
		{
			fNearestDis = fDistance;
			iNearest = i;
		}
	}
	
	// No loot spawn was matching conditions
	if(iNearest == -1)
		return -1;
	
	// Delete loot if a clean spawn was requested
	if(bRemove && g_nLootSpawns[iNearest][SpawnInUse])
		LootDelete(g_nLootSpawns[iNearest][SpawnEntity], 0, LS_ACTION_REPLACE);
	
	return iNearest;
}

bool SpawnLoot(int spawnID, int iGroup)
{
	int iEntity = CreateEntityByName("prop_physics_override");
	
	if (iEntity == -1)
		return false;
	
	char model[256];
	GetArrayString(g_aLootModels, iGroup, model, 256);
	
	SetEntityModel(iEntity, model);
	
	int iFlags = GetArrayCell(g_aFlags, iGroup);
	
	if(iFlags & LS_FLAG_TOUCH)
	{
		SDKHook(iEntity, SDKHook_StartTouch, LootStartTouch);
		SDKHook(iEntity, SDKHook_EndTouch, LootEndTouch);
	}
	
	if(iFlags & LS_FLAG_NOSHADOWS)
		DispatchKeyValue(iEntity, "disablereceiveshadows", "1");
		
	if(iFlags & LS_FLAG_NORECEIVESHADOWS)
		DispatchKeyValue(iEntity, "disableshadows", "1");
	
	DispatchSpawn(iEntity);
	
	SetEntProp(iEntity, Prop_Data, "m_takedamage", DAMAGE_NO, 1);
	
	g_nLootSpawns[spawnID][SpawnInUse] = true;
	g_nLootSpawns[spawnID][SpawnEntity] = iEntity;
	
	g_nLoot[iEntity][LootSpawnID] = spawnID;
	g_nLoot[iEntity][LootGroup] = iGroup;
	g_nLoot[iEntity][LootFlags] = iFlags;
	
	g_nLoot[iEntity][LootSpawned] = GetTime();
	
	float fPos[3];
	
	fPos[0] = g_nLootSpawns[spawnID][SpawnPosX];
	fPos[1] = g_nLootSpawns[spawnID][SpawnPosY];
	fPos[2] = g_nLootSpawns[spawnID][SpawnPosZ] + 32.0;
	
	float fAngle[3];
	fAngle[1] = GetRandomFloat(0.0, 360.0);
	
	TeleportEntity(iEntity, fPos, fAngle, NULL_VECTOR);
	
	Action result = OnLootSpawned(iEntity, iGroup, LS_ACTION_SPAWN);
	
	if (result == Plugin_Handled || result == Plugin_Stop)
	{
		LootDelete(iEntity, 0, LS_ACTION_NOSPAWN);
		return false;
	}
	
	return true;
}

int SpawnLootEx(int iGroup, float fPos[3])
{
	int iEntity = CreateEntityByName("prop_physics_override");
	
	if (iEntity == -1)
		return -1;
	
	char model[256];
	GetArrayString(g_aLootModels, iGroup, model, 256);
	
	SetEntityModel(iEntity, model);
	
	int iFlags = GetArrayCell(g_aFlags, iGroup);
	
	if(iFlags & LS_FLAG_TOUCH)
	{
		SDKHook(iEntity, SDKHook_StartTouch, LootStartTouch);
		SDKHook(iEntity, SDKHook_EndTouch, LootEndTouch);
	}
	
	if(iFlags & LS_FLAG_NOSHADOWS)
		DispatchKeyValue(iEntity, "disablereceiveshadows", "1");
		
	if(iFlags & LS_FLAG_NORECEIVESHADOWS)
		DispatchKeyValue(iEntity, "disableshadows", "1");
	
	DispatchSpawn(iEntity);
	
	SetEntProp(iEntity, Prop_Data, "m_takedamage", DAMAGE_NO, 1);
	
	g_nLoot[iEntity][LootSpawnID] = -1;
	g_nLoot[iEntity][LootGroup] = iGroup;
	g_nLoot[iEntity][LootFlags] = iFlags;
	g_nLoot[iEntity][LootSpawned] = GetTime();
	
	fPos[2] += 32.0;
	
	float fAngle[3];
	fAngle[1] = GetRandomFloat(0.0, 360.0);
	
	TeleportEntity(iEntity, fPos, fAngle, NULL_VECTOR);
	
	Call_StartForward(g_OnLootSpawned);
	Call_PushCell(iEntity);
	Call_PushCell(iGroup);
	Call_Finish();
	
	return iEntity;
}

/* Timer */

void RestartTimer()
{
	if(g_hTimer != null)
		CloseHandle(g_hTimer);
	
	g_hTimer = CreateTimer(g_fCheckInterval, Timer_CheckSpawns, _, TIMER_REPEAT | TIMER_FLAG_NO_MAPCHANGE);
}

public Action Timer_CheckSpawns(Handle timer, any data)
{
	// Stop the timer if idle mode has been activated
	if(!g_bIdle)
	{
		CheckSpawns(false);
	}
	
	return Plugin_Continue;
}

/* ConVars */

void CreateConVars()
{
	CreateConVar("zcore_lootspawner_version", PLUGIN_VERSION, "ZCore: Loot-Spawner version", FCVAR_DONTRECORD | FCVAR_SPONLY | FCVAR_REPLICATED | FCVAR_NOTIFY);
	
	g_cvCheckInterval = CreateConVar("zcore_lootspawner_check_interval", "1.0", "Delay between loot spawn checks.");
	g_fCheckInterval = GetConVarFloat(g_cvCheckInterval);
	HookConVarChange(g_cvCheckInterval, Action_OnSettingsChange);
	
	g_cvSpawnsMax = CreateConVar("zcore_lootspawner_spawn_max", "10", "Max loot to spawn on the map during the round.");
	g_iSpawnsMax = GetConVarInt(g_cvSpawnsMax);
	HookConVarChange(g_cvSpawnsMax, Action_OnSettingsChange);
	
	g_cvSpawnsMaxRoundStart = CreateConVar("zcore_lootspawner_spawn_max_roundstart", "10", "Max items to spawn at roundstart.");
	g_iSpawnsMaxRoundStart = GetConVarInt(g_cvSpawnsMaxRoundStart);
	HookConVarChange(g_cvSpawnsMaxRoundStart, Action_OnSettingsChange);
	
	g_cvMinDistance = CreateConVar("zcore_lootspawner_min_distance", "0.0", "Min range to nearest player to allow spawning loot (0.0 to disable).");
	g_fMinDistance = GetConVarFloat(g_cvMinDistance);
	HookConVarChange(g_cvMinDistance, Action_OnSettingsChange);
	
	AutoExecConfig(true, "lootspawner", "zcore");	
}

public void Action_OnSettingsChange(Handle cvar, const char[] oldvalue, const char[] newvalue)
{
	if (cvar == g_cvCheckInterval)
	{
		g_fCheckInterval = StringToFloat(newvalue);
		RestartTimer();
	}
	
	else if (cvar == g_cvSpawnsMax)
		g_iSpawnsMax = StringToInt(newvalue);
		
	else if (cvar == g_cvSpawnsMaxRoundStart)
		g_iSpawnsMaxRoundStart = StringToInt(newvalue);
		
	else if (cvar == g_cvMinDistance)
		g_fMinDistance = StringToFloat(newvalue);
}

/* Loot Interaction*/

public Action OnPlayerRunCmd(int iClient, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2])
{
	if(!(buttons & IN_USE))
		return Plugin_Continue;
	
	int iEntity = GetClientAimTarget(iClient, false);
	
	if(iEntity < MaxClients)
		return Plugin_Continue;
	
	if(!Entity_InRange(iClient, iEntity, 200.0))
		return Plugin_Continue;
		
	if(g_nLoot[iEntity][LootFlags] & LS_FLAG_USE)
		OnLootAction(iClient, iEntity, g_nLoot[iEntity][LootGroup], LS_ACTION_USE);
			
	return Plugin_Continue;
}

public Action LootStartTouch(int iEntity, int iClient)
{
	if (!Entity_IsPlayer(iClient))
		return Plugin_Continue;
	
	// Player start touch loot
	Action result = OnLootAction(iClient, iEntity, g_nLoot[iEntity][LootGroup], LS_ACTION_STARTTOUCH);
	if(result != Plugin_Handled && result != Plugin_Stop)
	{
		// Loot should break on touch
		if(g_nLoot[iEntity][LootFlags] & LS_FLAG_BREAK_STARTTOUCH)
		{
			// Player breaks loot
			OnLootAction(iClient, iEntity, g_nLoot[iEntity][LootGroup], LS_ACTION_BREAK);
			LootDelete(iEntity, iClient, LS_ACTION_BREAK);
		}
	}
	
	return Plugin_Continue;
}

public Action LootEndTouch(int iEntity, int iClient)
{
	if (!Entity_IsPlayer(iClient))
		return Plugin_Continue;
	
	// Player end touch loot
	Action result = OnLootAction(iClient, iEntity, g_nLoot[iEntity][LootGroup], LS_ACTION_ENDTOUCH);
	if(result != Plugin_Handled && result != Plugin_Stop)
	{
		// Loot should break on touch
		if(g_nLoot[iEntity][LootFlags] & LS_FLAG_BREAK_ENDTTOUCH)
		{
			// Player breaks loot
			LootDelete(iEntity, iClient, LS_ACTION_BREAK);
		}
	}
	
	return Plugin_Continue;
}

/* Forwards */

void CreateForwards()
{
	g_OnPluginStart = CreateGlobalForward("ZCore_LootSpawner_OnPluginStart", ET_Ignore);
	g_OnLootSpawnThink = CreateGlobalForward("ZCore_LootSpawner_OnLootSpawnThink", ET_Event, Param_Cell, Param_CellByRef);
	g_OnLootSpawnPre = CreateGlobalForward("ZCore_LootSpawner_OnLootSpawnPre", ET_Event, Param_Cell, Param_Array, Param_CellByRef);
	g_OnLootSpawned = CreateGlobalForward("ZCore_LootSpawner_OnLootSpawned", ET_Event, Param_Cell, Param_Cell, Param_Cell);
	g_OnLootAction = CreateGlobalForward("ZCore_LootSpawner_OnLootAction", ET_Event, Param_Cell, Param_Cell, Param_Cell, Param_Cell);
	g_OnLootRemoved = CreateGlobalForward("ZCore_LootSpawner_OnLootAction", ET_Event, Param_Cell, Param_Cell, Param_Cell, Param_Cell);
}

Action OnLootSpawnThink(int iGroup, int failStreak)
{
	Action result;
	
	Call_StartForward(g_OnLootSpawnThink);
	Call_PushCell(iGroup);
	Call_PushCellRef(failStreak);
	Call_Finish(result);
	
	return result;
}

Action OnLootSpawnPre(int iGroup, float fPos[3], int failStreak)
{
	Action result;
	
	Call_StartForward(g_OnLootSpawnPre);
	Call_PushCell(iGroup);
	Call_PushArray(fPos, 3);
	Call_PushCellRef(failStreak);
	Call_Finish(result);
	
	return result;
}

Action OnLootSpawned(int iEntity, int iGroup, int iAction)
{
	Action result;
	
	Call_StartForward(g_OnLootSpawned);
	Call_PushCell(iEntity);
	Call_PushCell(iGroup);
	Call_PushCell(iAction);
	Call_Finish(result);
	
	return result;
}

Action OnLootAction(int iClient, int iEntity, int iGroup, int iAction)
{
	Action result;
	
	Call_StartForward(g_OnLootAction);
	Call_PushCell(iClient);
	Call_PushCell(iEntity);
	Call_PushCell(iGroup);
	Call_PushCell(iAction);
	Call_Finish(result);
	
	return result;
}

Action OnLootRemoved(int iClient, int iEntity, int iGroup, int iAction)
{
	Action result;
	
	Call_StartForward(g_OnLootRemoved);
	Call_PushCell(iClient);
	Call_PushCell(iEntity);
	Call_PushCell(iGroup);
	Call_PushCell(iAction);
	Call_Finish(result);
	
	return result;
}

/* Other */

float GetClosestPlayerDistance(float pos[3])
{
	float fDistance;
	float fClosestDistance = -1.0;
	
	LoopAlivePlayers(i)
	{
		float fTargetPos[3];
		GetClientAbsOrigin(i, fTargetPos);
		
		if (fTargetPos[0] == 0.0 && fTargetPos[1] == 0.0 && fTargetPos[2] == 0.0)
			continue;
		
		fDistance = GetVectorDistance(pos, fTargetPos);
		
		if (fDistance < fClosestDistance || fClosestDistance == -1.0)
			fClosestDistance = fDistance;
	}
	
	return fClosestDistance;
}

/* Natives */

void CreateNatives()
{
	CreateNative("ZCore_LootSpawner_RegisterLootType", Native_RegisterLootType);
	CreateNative("ZCore_LootSpawner_DeregisterLootType", Native_DeregisterLootType);
	CreateNative("ZCore_LootSpawner_GetType", Native_GetType);
	CreateNative("ZCore_LootSpawner_RemoveLoot", Native_RemoveLoot);
	CreateNative("ZCore_LootSpawner_WipeEntitysAll", Native_WipeEntitysAll);
	CreateNative("ZCore_LootSpawner_WipeEntitysByType", Native_WipeEntitysByType);
	CreateNative("ZCore_LootSpawner_ForceSpawn", Native_ForceSpawn);
	CreateNative("ZCore_LootSpawner_ForceSpawnNear", Native_ForceSpawnNear);
}

public int Native_RegisterLootType(Handle plugin, int numParams)
{
	char name[32];
	GetNativeString(1, name, 32);
	
	char model[256];
	GetNativeString(2, model, 256);
	
	int health = GetNativeCell(3);
	int ttl = GetNativeCell(4);
	
	int weight = GetNativeCell(5);
	int roundStartWeight = GetNativeCell(6);
	int maxSpawned = GetNativeCell(7);
	
	int iFlags = GetNativeCell(8);
	int minDistance = GetNativeCell(9);
	int maxDistance = GetNativeCell(10);
	
	// Precache model
	if (!IsModelPrecached(model))
		PrecacheModel(model, true);
	
	// Check if group exist
	int iGroup = -1;
	for (int i = 0; i < GetArraySize(g_aLootGroups); i++)
	{
		char name2[32];
		GetArrayString(g_aLootGroups, i, name2, 32);
		
		if (!StrEqual(name, name2, false))
			continue;
		
		iGroup = i;
	}
	
	// not found, register a new loot group
	if (iGroup == -1)
	{
		iGroup = PushArrayString(g_aLootGroups, name);
		PushArrayString(g_aLootModels, model);
		
		PushArrayCell(g_aMaxSpawned, maxSpawned);
		PushArrayCell(g_aSpawnDistanceMin, minDistance);
		PushArrayCell(g_aSpawnDistanceMax, maxDistance);
		
		PushArrayCell(g_aLootWeight, weight);
		for (int i = 0; i < weight; i++)
			PushArrayCell(g_aLootTable, iGroup);
		
		PushArrayCell(g_aLootWeightRoundStart, roundStartWeight);
		for (int i = 0; i < roundStartWeight; i++)
			PushArrayCell(g_aLootTableRoundStart, iGroup);
		
		PushArrayCell(g_aTTL, ttl);
		PushArrayCell(g_aHealth, health);
		
		PushArrayCell(g_aFlags, iFlags);
	}
	
	// overwrite old settings for this loot group
	
	else
	{
		// Overwrite model path
		
		SetArrayString(g_aLootModels, iGroup, model);
		
		// Overwrite max spawnewd
		
		SetArrayCell(g_aMaxSpawned, iGroup, maxSpawned);
		
		// Weight
		
		SetArrayCell(g_aLootWeight, iGroup, weight);
		
		for (int i = 0; i < GetArraySize(g_aLootTable) - 1; i++)
		{
			if (GetArrayCell(g_aLootTable, i) == iGroup)
				RemoveFromArray(g_aLootTable, i);
		}
		
		for (int i = 0; i < weight; i++)
			PushArrayCell(g_aLootTable, iGroup);
		
		// Weight (Roundstart)
		
		SetArrayCell(g_aLootWeightRoundStart, iGroup, roundStartWeight);
		
		for (int i = 0; i < GetArraySize(g_aLootTableRoundStart) - 1; i++)
		{
			if (GetArrayCell(g_aLootTableRoundStart, i) == iGroup)
				RemoveFromArray(g_aLootTableRoundStart, i);
		}
		
		for (int i = 0; i < roundStartWeight; i++)
			PushArrayCell(g_aLootTableRoundStart, iGroup);
		
		// TTL
		
		SetArrayCell(g_aTTL, iGroup, ttl);
		
		// Health
		
		SetArrayCell(g_aHealth, iGroup, health);
			
		// Flags
		
		SetArrayCell(g_aFlags, iGroup, iFlags);
		
		// Distance
		
		SetArrayCell(g_aSpawnDistanceMin, iGroup, minDistance);
		SetArrayCell(g_aSpawnDistanceMax, iGroup, maxDistance);
	}
	
	return iGroup;
}

public int Native_DeregisterLootType(Handle plugin, int numParams)
{
	char name[32];
	GetNativeString(1, name, 32);
	
	for (int iGroup = 0; iGroup < GetArraySize(g_aLootGroups); iGroup++)
	{
		char name2[32];
		GetArrayString(g_aLootGroups, iGroup, name2, 32);
		
		if (!StrEqual(name, name2, false))
			continue;
		
		// Name
		
		RemoveFromArray(g_aLootGroups, iGroup);
		
		// Model
		
		RemoveFromArray(g_aLootModels, iGroup);
		
		// Weight
		
		RemoveFromArray(g_aLootWeight, iGroup);
		for (int i = 0; i < GetArraySize(g_aLootTable) - 1; i++)
		{
			if (GetArrayCell(g_aLootTable, i) == iGroup)
				RemoveFromArray(g_aLootTable, i);
		}
		
		// Roundstart weight
		
		RemoveFromArray(g_aLootWeightRoundStart, iGroup);
		for (int i = 0; i < GetArraySize(g_aLootTableRoundStart) - 1; i++)
		{
			if (GetArrayCell(g_aLootTableRoundStart, i) == iGroup)
				RemoveFromArray(g_aLootTableRoundStart, i);
		}
		
		// Max spawned
		
		RemoveFromArray(g_aMaxSpawned, iGroup);
		
		// Spawn distance
		
		RemoveFromArray(g_aSpawnDistanceMin, iGroup);
		RemoveFromArray(g_aSpawnDistanceMax, iGroup);
		
		// TTL
		
		RemoveFromArray(g_aFlags, iGroup);
		
		// Health
		
		RemoveFromArray(g_aFlags, iGroup);
		
		// Flags
		
		RemoveFromArray(g_aFlags, iGroup);
		
		return iGroup;
	}
	
	return -1;
}

public int Native_GetType(Handle plugin, int numParams)
{
	int iEntity = GetNativeCell(1);
	return g_nLoot[iEntity][LootGroup];
}

public int Native_RemoveLoot(Handle plugin, int numParams)
{
	int iEntity = GetNativeCell(1);
	int iClient = GetNativeCell(2);
	int iAction = GetNativeCell(3);
	LootDelete(iEntity, iClient, iAction);
}

public int Native_WipeEntitysAll(Handle plugin, int numParams)
{
	LootDeleteAll();
}

public int Native_WipeEntitysByType(Handle plugin, int numParams)
{
	int iGroup = GetNativeCell(1);
	LootDeleteGroup(iGroup);
} 

public int Native_ForceSpawn(Handle plugin, int numParams)
{
	int iGroup = GetNativeCell(1);
	
	float fPos[3];
	GetNativeArray(2, fPos, 3);
	
	int iClient = GetNativeCell(3);
	int iAction = GetNativeCell(4);
	
	int iEntity = SpawnLootEx(iGroup, fPos);
	
	if(iEntity <= 0)
		return -1;
	
	Action result = OnLootSpawned(iEntity, iGroup, iAction);
	
	if (result == Plugin_Handled || result == Plugin_Stop)
	{
		LootDelete(iEntity, iClient, iAction);
		iEntity = -1;
	}
	
	return iEntity;
}

public int Native_ForceSpawnNear(Handle plugin, int numParams)
{
	int iGroup = GetNativeCell(1);
	
	float fPos[3];
	GetNativeArray(2, fPos, 3);
	
	int iClient = GetNativeCell(3);
	int iAction = GetNativeCell(4);
	
	float fRangeMin = GetNativeCell(5);
	float fRangeMax = GetNativeCell(6);
	
	bool bNextFree = GetNativeCell(7);
	bool bRemove = GetNativeCell(8);
	
	/* Check Spawns */
	
	int iSpawnID = GetSpawnNear(fPos, fRangeMin, fRangeMax, true, false);
	
	if (bRemove && iSpawnID < 0)
		iSpawnID = GetSpawnNear(fPos, fRangeMin, fRangeMax, bNextFree, bRemove);
	
	int iEntity = SpawnLoot(iSpawnID, iGroup);
	
	if(iEntity <= 0)
		return -1;
		
	Action result = OnLootSpawned(iEntity, iGroup, iAction);
	
	if (result == Plugin_Handled || result == Plugin_Stop)
	{
		LootDelete(iEntity, iClient, iAction);
		iEntity = -1;
	}
	
	return iEntity;
}