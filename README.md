This plugin manages loot spawns for lootgroups which can be registered by other plugins. 
This means: It doesn't do anything as long no other plugin registers a loot group to it.

**Install Instructions:**
- Upload this plugin and it's config to your server
- Adjust the master config in "[/cfg/zcore/lootspawner.cfg](http://gitlab.com/Zipcore/ZCore-LootSpawner/blob/master/cfg/zcore/lootspawner.cfg)"
- create and grant write permissions to this directory: "addons/sourcemod/configs/lootspawns/"

**Commands:**
- zcore_loot_add # Adds a spawn points at your position
- zcore_loot_show # Shows spawn points which are in range
- zcore_loot_delete # Deletes the nearest spawn point
- zcore_loot_delete_prev # Deletes the last spawn point
- zcore_loot_reload # Respawns all loot
- zcore_loot_debug # List all registered loot groups